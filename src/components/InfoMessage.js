import React from 'react';

const InfoMessage = ({ title, subtitle }) => {
  return (
    <div className="info-message">
      <h3>{title}</h3>
      <h6 className="pt-2">{subtitle}</h6>
    </div>
  );
};

export default InfoMessage;
