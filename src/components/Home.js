import React, { memo } from 'react';

// components
import FeaturedMovie from './containers/FeaturedMovie.container';
import Search from './containers/Search.container';

const Home = () => {
  return (
    <div>
      <FeaturedMovie />
      <Search />
    </div>
  );
};

export default memo(Home);
