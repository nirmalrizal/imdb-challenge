import {
  UPDATE_MOVIES,
  UPDATE_MOVIE_DETAIL,
  ADD_MOVIES,
  FETCHING_MOVIES,
} from './types';

export const addMovies = movies => {
  return {
    type: ADD_MOVIES,
    payload: movies,
  };
};

export const updateMovies = movies => {
  return {
    type: UPDATE_MOVIES,
    payload: movies,
  };
};

export const fetchingMovies = page => {
  return {
    type: FETCHING_MOVIES,
    page,
  };
};

export const updateMovieDetail = (movieId, movieDetail) => {
  return {
    type: UPDATE_MOVIE_DETAIL,
    movieId,
    movieDetail,
  };
};
