import React, { memo } from 'react';

// components
import Loading from './Loading';
import InfoMessage from './InfoMessage';
import LoadMoreButton from './LoadMoreButton';

import defaultImage from '../assets/movie.png';

const SearchResult = ({ movies, loadMore, keyword }) => {
  if (movies.loading && movies.data.length === 0) {
    return <Loading top />;
  }
  if (keyword.length < 3) {
    return (
      <InfoMessage title="No active search" subtitle="Type to start search" />
    );
  }
  if (movies.data && movies.data.length === 0) {
    return (
      <InfoMessage
        title="No result found"
        subtitle="Try with another keyword"
      />
    );
  }

  const renderMovies = () => {
    return movies.data.map(movie => {
      const { Title, Year, Poster, imdbID } = movie;
      return (
        <div className="col-md-3 col-sm-6 movie-card" key={imdbID}>
          <img
            className="img-responsive"
            src={!Poster || Poster === 'N/A' ? defaultImage : Poster}
            alt={Title}
          />
          <div className="mt-2">
            <h5 className="mb-0">{Title}</h5>
            <span>{Year}</span>
          </div>
        </div>
      );
    });
  };

  return (
    <div className="search-result-container">
      <div className="row m-0">
        <div className="col-md-12">
          <div className="container pt-4 pb-4">
            <div className="row">{renderMovies()}</div>
            {movies.loading ? (
              <Loading top />
            ) : (
              <LoadMoreButton loadMore={loadMore} />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(SearchResult);
