import React from 'react';

const Header = () => {
  return (
    <nav className="navbar navbar-dark bg-dark">
      <div className="container pb-2 pt-2">
        <a
          className="navbar-brand d-flex align-items-center header-title"
          href="/">
          <i className="fas fa-film movie-icon" /> IMDB Challenge
        </a>
      </div>
    </nav>
  );
};

export default Header;
