import React, { memo } from 'react';

const SearchInput = ({ keyword, handleInputChange }) => {
  return (
    <div className="row m-0">
      <div className="col-md-12" id="search-input-container">
        <input
          type="text"
          id="search-input"
          placeholder="Search"
          value={keyword}
          onChange={handleInputChange}
        />
      </div>
    </div>
  );
};

export default memo(SearchInput);
