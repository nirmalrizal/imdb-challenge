import React from 'react';

const Loading = ({ top }) => {
  return (
    <div className={`loading ${top && 'loading-top'}`}>
      <i className="fa fa-spin fa-circle-notch loading-spinner" />
    </div>
  );
};

export default Loading;
