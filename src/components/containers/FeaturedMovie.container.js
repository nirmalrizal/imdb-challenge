import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import Component from '../FeaturedMovie';

// actions
import { updateMovieDetail } from '../../store/actions';

// utils
import Api from '../../utils/api';
import StringUtil from '../../utils/strings';
import { MIN_YEAR, MAX_YEAR } from '../../utils/config';

const FeaturedMovieContainer = props => {
  const [featuredMovie, setFeaturedMovie] = useState(null);

  useEffect(() => {
    const fetchRandomMovie = async () => {
      const randomYear = StringUtil.getRandomInclusiveNumber(
        MIN_YEAR,
        MAX_YEAR,
      );
      const response = await Api.fetchMovieDetail({
        t: 'man',
        y: randomYear,
      });
      if (response && response.status === 200 && response.data) {
        setFeaturedMovie(response.data);
        props.updateMovieDetail(response.data.imdbID, response.data);
      }
    };

    fetchRandomMovie();
  }, []);

  return <Component movie={featuredMovie} />;
};

const mapDispatchToProps = {
  updateMovieDetail,
};

export default connect(null, mapDispatchToProps)(FeaturedMovieContainer);
