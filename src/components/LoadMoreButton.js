import React from 'react';

const LoadMoreButton = ({ loadMore }) => {
  return (
    <button
      className="btn btn-warning btn-block load-more"
      type="button"
      onClick={loadMore}>
      Load More
    </button>
  );
};

export default LoadMoreButton;
