import {
  UPDATE_MOVIE_DETAIL,
  ADD_MOVIES,
  UPDATE_MOVIES,
  FETCHING_MOVIES,
} from './types';

const initialState = {
  movies: {
    loading: false,
    data: [],
  },
  movieDetails: {},
};

const imdbReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_MOVIE_DETAIL:
      return {
        ...state,
        movieDetails: {
          ...state.movieDetails,
          [action.movieId]: action.movieDetail,
        },
      };
    case ADD_MOVIES:
      return {
        ...state,
        movies: {
          loading: false,
          data: action.payload,
        },
      };
    case UPDATE_MOVIES:
      return {
        ...state,
        movies: {
          loading: false,
          data: [...state.movies.data, ...action.payload],
        },
      };
    case FETCHING_MOVIES:
      return {
        ...state,
        movies: {
          loading: true,
          data: action.page === 1 ? [] : [...state.movies.data],
        },
      };
    default:
      return state;
  }
};

export default imdbReducer;
