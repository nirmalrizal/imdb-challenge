import React, { memo } from 'react';

import defaultImage from '../assets/movie.png';

// components
import Badges from './Badges';
import Loading from './Loading';

const FeaturedMovie = ({ movie }) => {
  if (movie === null) {
    return <Loading />;
  }

  const {
    Poster,
    Plot,
    Actors,
    Genre,
    Runtime,
    imdbRating,
    Year,
    Title,
    Director,
  } = movie;

  const moviePoster = Poster ? Poster.replace('SX300', 'SX1200') : defaultImage;
  const genres = Genre.split(',').map(genre => genre.trim());

  return (
    <div className="col-md-12 p-0">
      <div
        id="featured-movie-container"
        style={{
          backgroundImage: `url(${moviePoster})`,
        }}>
        <div id="featured-movie-detail-container">
          <img src={Poster} alt={Title} id="movie-poster" />
          <div id="featured-movie-detail">
            <h2>{Title}</h2>
            <h6 className="mt-4">PLOT</h6>
            <p>{Plot}</p>
            <div id="movie-minor-details">
              <div>
                <h6 className="mt-4">IMDB RATING</h6>
                <div>
                  <meter value={imdbRating} min="0" max="10" />
                  <span className="imdb-rating">{imdbRating}</span>
                </div>
              </div>
              <div>
                <h6 className="mt-4">RUNTIME</h6>
                <span>{Runtime}</span>
              </div>
              <div>
                <h6 className="mt-4">YEAR</h6>
                <span>{Year}</span>
              </div>
            </div>
            <div id="director-genre-detail">
              <div>
                <h6 className="mt-4">DIRECTOR</h6>
                <p>{Director}</p>
              </div>
              <div className="ml-5">
                <h6 className="mt-4">GENRE</h6>
                <Badges items={genres} />
              </div>
            </div>
            <h6 className="mt-4">CAST</h6>
            <p>{Actors}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(FeaturedMovie);
