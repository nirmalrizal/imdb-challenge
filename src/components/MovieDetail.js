import React from 'react';
import { useParams } from 'react-router-dom';

const MovieDetail = () => {
  const { movieId } = useParams();
  return (
    <div>
      <h1>MovieDetail - {movieId}</h1>
    </div>
  );
};

export default MovieDetail;
