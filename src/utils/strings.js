const generateRandomAlphabet = () => {
  const randomNumber = getRandomInclusiveNumber(97, 122);
  return String.fromCharCode(randomNumber);
};

const getRandomInclusiveNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export default {
  generateRandomAlphabet,
  getRandomInclusiveNumber,
};
