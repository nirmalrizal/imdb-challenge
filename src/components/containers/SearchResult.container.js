import React from 'react';
import { connect } from 'react-redux';

import Component from '../SearchResult';

const SearchResultContainer = props => {
  return <Component {...props} />;
};

const mapStateToProps = state => {
  return {
    movies: state.movies,
  };
};

export default connect(mapStateToProps)(SearchResultContainer);
