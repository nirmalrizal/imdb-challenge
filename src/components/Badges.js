import React, { memo } from 'react';

const Badges = ({ items }) => {
  return items.map(item => {
    return (
      <span className="badge badge-warning genre-badge" key={item}>
        {item}
      </span>
    );
  });
};

export default memo(Badges);
