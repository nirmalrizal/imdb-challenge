import axios from 'axios';

// config
import { OMDB_API_KEY, API_BASE_URL } from './config';

const fetchMovies = async (keyword, callToken, page = 1, filterParams = {}) => {
  const moviesResponse = await axios
    .get(API_BASE_URL, {
      params: {
        s: keyword,
        type: filterParams.type,
        y: filterParams.year,
        page,
        apikey: OMDB_API_KEY,
      },
      cancelToken: callToken.token,
    })
    .catch(err => err);
  return moviesResponse;
};

const fetchMovieDetail = async movieParams => {
  const moviesResponse = await axios
    .get(API_BASE_URL, {
      params: {
        ...movieParams,
        apikey: OMDB_API_KEY,
      },
    })
    .catch(err => err);
  return moviesResponse;
};

export default {
  fetchMovies,
  fetchMovieDetail,
};
