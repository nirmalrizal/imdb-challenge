import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import '@fortawesome/fontawesome-free/js/all';

import App from '@/App';

import 'bootstrap/dist/css/bootstrap.min.css';
import '@/style.css';

// Redux store
import store from '@/store';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('__root'),
);

if (module.hot) {
  module.hot.accept();
}
