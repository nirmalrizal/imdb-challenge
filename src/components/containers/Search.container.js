import React, { useState, useRef } from 'react';
import { connect } from 'react-redux';
import { CancelToken } from 'axios';

// utils
import Api from '../../utils/api';

// actions
import { addMovies, updateMovies, fetchingMovies } from '../../store/actions';

// components
import SearchInput from '../SearchInput';
import SearchResult from './SearchResult.container';

const Search = props => {
  const [keyword, setKeyword] = useState('');
  const [page, setPage] = useState(1);
  const callToken = useRef(null);

  const handleSearch = async (keywordParam, pageParam) => {
    if (callToken.current !== null) {
      callToken.current.call1.cancel(
        `Old request cancelled because of : ${keywordParam}`,
      );
      callToken.current.call2.cancel(
        `Old request cancelled because of : ${keywordParam}`,
      );
    }
    callToken.current = {
      call1: CancelToken.source(),
      call2: CancelToken.source(),
    };
    if (keywordParam.length >= 3) {
      props.fetchingMovies(pageParam);
      const callOneResponse = await Api.fetchMovies(
        keywordParam,
        callToken.current.call1,
        pageParam,
      );
      const callTworesponse = await Api.fetchMovies(
        keywordParam,
        callToken.current.call2,
        pageParam + 1,
      );

      if (
        callOneResponse &&
        callOneResponse.status === 200 &&
        callOneResponse.data &&
        callOneResponse.data.Search
      ) {
        if (pageParam === 1) {
          props.addMovies(callOneResponse.data.Search);
        } else {
          props.updateMovies(callOneResponse.data.Search);
        }
      } else {
        props.updateMovies([]);
      }
      if (
        callTworesponse &&
        callTworesponse.status === 200 &&
        callTworesponse.data &&
        callTworesponse.data.Search
      ) {
        props.updateMovies(callTworesponse.data.Search);
      } else {
        props.updateMovies([]);
      }
    } else {
      props.addMovies([]);
    }
  };

  const handleInputChange = e => {
    const targetValue = e.target.value;
    setKeyword(targetValue);
    setPage(1);
    setTimeout(() => {
      handleSearch(targetValue, 1);
    }, 500);
  };

  const loadMore = () => {
    setPage(page + 2);
    handleSearch(keyword, page + 2);
  };

  return (
    <>
      <SearchInput keyword={keyword} handleInputChange={handleInputChange} />
      <SearchResult loadMore={loadMore} keyword={keyword} />
    </>
  );
};

const mapDispatchToProps = {
  addMovies,
  updateMovies,
  fetchingMovies,
};

export default connect(null, mapDispatchToProps)(Search);
